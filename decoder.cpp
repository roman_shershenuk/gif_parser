#include "decoder.h"

#include <QDebug>

Decoder::Decoder(Image_Data_t &image_data, std::vector<uint8_t> &index_table):
    m_data_blocks(image_data.data_blocks),
    m_index_table(index_table),
    m_dict_size(1 << (image_data.lzw_code_size + 1)),
    m_basic_table_size(1 << image_data.lzw_code_size),
    m_clear_code(static_cast<uint16_t>(1 << image_data.lzw_code_size)),
    m_end_of_image(static_cast<uint16_t>((1 << image_data.lzw_code_size) + 1)),
    m_code_size(image_data.lzw_code_size + 1) {

    init_dict();
    clear_dict_ind();
}

uint16_t Decoder::get_next_code(const size_t code_size) {
    return (m_data_blocks.get_bit_group(code_size));
}

void Decoder::init_dict() {
    m_dict.resize(m_dict_size);

    for(size_t i = 0; i < m_basic_table_size; ++i) {
        m_dict[i].push_back(static_cast<uint8_t>(i));
     }

    m_dict[m_clear_code].push_back(0);
    m_dict[m_end_of_image].push_back(0);
}

void Decoder::inc_dict_size() {
    ++m_code_size;
    m_dict_size = (1 << m_code_size);
    m_dict.resize(m_dict_size);
}

void Decoder::clear_dict() {
    m_dict.clear();
}

void Decoder::clear_dict_ind() {
    m_dict_ind = m_end_of_image + 1;
}

void Decoder::inc_dict_ind() {
    ++m_dict_ind;

    if(m_dict_ind > (m_dict_size - 1)) {
        inc_dict_size();
    }
}

void Decoder::write_prev_code(const uint16_t code) {
    for(size_t i = 0; i < m_dict[code].size(); ++i) {
        m_dict[m_dict_ind].push_back(m_dict[code][i]);
    }
}

void Decoder::write_next_code(const uint16_t code) {
    m_dict[m_dict_ind].push_back(m_dict[code][0]);
}

void Decoder::copy_to_index_table(const uint16_t code) {
    for(size_t i = 0; i < m_dict[code].size(); ++i) {
        m_index_table.push_back(static_cast<uint8_t>(m_dict[code][i]));
    }
}

bool Decoder::is_entry_exist(const uint16_t code) {
    return m_dict[code].size() > 0;
}

void Decoder::decode() {
    uint16_t next_code = get_next_code(m_code_size);
    const uint16_t bad_code = std::numeric_limits<uint16_t>::max();
    uint16_t prev_code = bad_code;

    while(next_code != m_end_of_image) {

        if(next_code == m_clear_code) {
            clear_dict();
            init_dict();
            clear_dict_ind();

            m_code_size = 9;
            m_dict_size = 1 << m_code_size;
            m_dict.resize(m_dict_size);

            prev_code = bad_code;
        }
        else {
            if(prev_code == bad_code) {
                copy_to_index_table(next_code);
                prev_code = next_code;
            }

            else if(is_entry_exist(next_code)) {
                copy_to_index_table(next_code);
                write_prev_code(prev_code);
                write_next_code(next_code);
                inc_dict_ind();
                prev_code = next_code;
            }

            else {
                write_prev_code(prev_code);
                m_dict[m_dict_ind].push_back(m_dict[m_dict_ind].data()[0]);
                copy_to_index_table(next_code);
                inc_dict_ind();
                prev_code = next_code;
            }
        }

        next_code = get_next_code(m_code_size);
    }
}
