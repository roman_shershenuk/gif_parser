#ifndef DICTIONARY_H
#define DICTIONARY_H

#include "utils.h"
#include "simple_bitarray.h"

class Decoder {
public:
    Decoder(Image_Data_t &image_data, std::vector<uint8_t> &index_table);
    void decode();
private:
    SimpleBitArray m_data_blocks;
    std::vector<uint8_t> &m_index_table;
    std::vector<std::vector<uint16_t>> m_dict;

    size_t m_dict_size;
    size_t m_basic_table_size;

    uint16_t m_clear_code;
    uint16_t m_end_of_image;
    uint16_t m_code_size;

    size_t m_dict_ind;

    uint16_t get_next_code(const size_t code_size);

    void init_dict();
    void inc_dict_size();
    void clear_dict();

    void clear_dict_ind();
    void inc_dict_ind();

    void write_prev_code(const uint16_t code);
    void write_next_code(const uint16_t code);
    void copy_to_index_table(const uint16_t code);
    bool is_entry_exist(const uint16_t code);
};

#endif // DICTIONARY_H
