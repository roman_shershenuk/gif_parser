#include <cassert>

#include "simple_bitarray.h"

SimpleBitArray::SimpleBitArray(std::vector<uint8_t> &data_blocks):
    m_data_blocks(data_blocks),
    m_position(0) {

    assert(m_data_blocks.size() != 0);
}

uint16_t SimpleBitArray::get_bit(const size_t index) {
    size_t byte_index = index / 8;
    size_t bit_index = index % 8;

    assert(byte_index < m_data_blocks.size());

    return ((m_data_blocks.data()[byte_index] >> bit_index) & 1);
}

uint16_t SimpleBitArray::get_bit_group(const size_t bit_n) {
    uint16_t bit_group = 0;

    for(size_t i = 0; i < bit_n; ++i) {
        bit_group |= (get_bit(m_position + i) << i);
    }

    m_position += bit_n;

    return bit_group;
}

size_t SimpleBitArray::get_curt_pos() {
    return  m_position;
}
