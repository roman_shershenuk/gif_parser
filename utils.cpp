#include <QDebug>

#include "utils.h"

int get_file_type_string(QDataStream &data_stream, char *string, const int string_len) {
    int read_len = data_stream.readRawData(string, (string_len - 1));
    string[(string_len - 1)] = 0;

    assert(strcmp("GIF89a", string) == 0);

    return read_len;
}

int get_screen_descriptor(QDataStream &data_stream, Screen_Descritor_t *screen_descriptor) {
    return data_stream.readRawData(static_cast<char *>(static_cast<void *>(screen_descriptor)), sizeof(Screen_Descritor_t));
}

bool is_gct(const uint8_t flags) {
    const uint8_t gct_mask = 0x80;
    return (flags & gct_mask);
}

int get_color_res(const uint8_t flags) {
    const uint8_t color_res_mask = 0x70;
    return (1 << (((flags & color_res_mask) >> 4) + 1));
}

bool is_sorted(const uint8_t flags) {
    const uint8_t sorted_flag_mask = 0x08;
    return (flags & sorted_flag_mask);
}

int get_number_of_bits_in_gct(const uint8_t flags) {
    const uint8_t gct_number_of_bits_mask = 0x07;
    return (1 << ((flags & gct_number_of_bits_mask) + 1));
}

int get_global_color_table(QDataStream &data_stream, std::vector<uint8_t> &global_color_table, int global_color_table_len) {
    global_color_table.resize(global_color_table_len);
    return data_stream.readRawData(static_cast<char *>(static_cast<void *>(global_color_table.data())), global_color_table_len);
}

int get_image_descriptor(QDataStream &data_stream, Image_Descriptor_t *image_descriptor) {
    return data_stream.readRawData(static_cast<char *>(static_cast<void *>(image_descriptor)), sizeof(Image_Descriptor_t));
}

int get_local_color_table(QDataStream &data_stream, std::vector<uint8_t> &local_color_table, int local_color_table_len) {
    local_color_table.resize(local_color_table_len);
    return data_stream.readRawData(static_cast<char *>(static_cast<void *>(local_color_table.data())), local_color_table_len);
}

uint8_t get_byte(QDataStream &data_stream) {
    uint8_t byte;
    assert(data_stream.readRawData(static_cast<char *>(static_cast<void *>(&byte)), 1) == 1);
    return byte;
}

void parse_image_data(QDataStream &data_stream, Image_Data_t &image_data) {

    const uint8_t IMAGE_DESCRIPTOR = 0x2C;
    const uint8_t TRAILER = 0x3B;

    uint8_t data_byte = 0x00;

    while(data_byte != TRAILER) {
        data_byte = get_byte(data_stream);

        switch(data_byte) {
            case IMAGE_DESCRIPTOR:
                qDebug() << get_image_descriptor(data_stream, &image_data.image_descriptor) << "bytes were received from GIF image descriptor" << '\n';

                if(is_lct(image_data.image_descriptor.flags)) {
                    // Для RGB
                    int local_color_table_len = 3 * get_number_of_bits_in_lct(image_data.image_descriptor.flags);
                    qDebug() << get_local_color_table(data_stream, image_data.local_color_table, local_color_table_len) << "bytes were received from GIF local color table" << '\n';
                }

                qDebug() << "LZW codes size:" << (image_data.lzw_code_size = get_byte(data_stream)) << '\n';

                qDebug() << read_sub_blocks(data_stream, image_data.data_blocks) << "bytes were received from GIF data blocks" << '\n';

            break;

            case TRAILER:
            break;

            default:
            break;
        }

    }
}

bool is_lct(const uint8_t flags) {
    const uint8_t lct_mask = 0x80;
    return (flags & lct_mask);
}

bool is_interlaced(const uint8_t flags) {
    const uint8_t interlace_mask = 0x40;
    return (flags & interlace_mask);
}

bool is_lcl_sorted(const uint8_t flags) {
    const uint8_t sorted_flag_mask = 0x20;
    return (flags & sorted_flag_mask);
}

int get_number_of_bits_in_lct(const uint8_t flags) {
    const uint8_t lct_number_of_bits_mask = 0x07;
    return (1 << ((flags & lct_number_of_bits_mask) + 1));
}


int read_sub_blocks(QDataStream &data_stream, std::vector<uint8_t> &data_blocks) {
    uint8_t block_size = 0;
    size_t data_length = 0;
    size_t index = 0;

    while(1) {
        block_size = get_byte(data_stream);

        if(block_size == 0) {
            break;
        }

        data_length += block_size;
        data_blocks.resize(data_length);
        assert(data_stream.readRawData(static_cast<char *>(static_cast<void *>((data_blocks.data() + index))), block_size) == block_size);
        index += block_size;
    }
    return static_cast<int>(data_blocks.size());
}
