#ifndef UTILS_H
#define UTILS_H

#include <QDataStream>

#pragma pack(push, 1)
struct Screen_Descritor_t {
    uint16_t width;
    uint16_t height;
    uint8_t flags;
    uint8_t background_color_index;
    uint8_t pixel_aspect_ratio;
};

struct Image_Descriptor_t {
    uint16_t image_left_position;
    uint16_t image_top_position;
    uint16_t image_width;
    uint16_t image_height;
    uint8_t flags;
};
#pragma pack(pop)

struct Image_Data_t {
    Image_Descriptor_t image_descriptor;
    std::vector<uint8_t> local_color_table;
    std::vector<uint8_t> data_blocks;
    uint8_t lzw_code_size;
};

int get_file_type_string(QDataStream &data_stream, char *string, const int string_len);
int get_screen_descriptor(QDataStream &data_stream, Screen_Descritor_t *screen_descriptor);

bool is_gct(const uint8_t flags);
int get_color_res(const uint8_t flags);
bool is_sorted(const uint8_t flags);
int get_number_of_bits_in_gct(const uint8_t flags);

int get_global_color_table(QDataStream &data_stream, std::vector<uint8_t> &global_color_table, int global_color_table_len);
int get_image_descriptor(QDataStream &data_stream, Image_Descriptor_t *image_descriptor);
int get_local_color_table(QDataStream &data_stream, std::vector<uint8_t> &local_color_table, int local_color_table_len);
uint8_t get_byte(QDataStream &data_stream);

void parse_image_data(QDataStream &data_stream, Image_Data_t &image_data);

bool is_lct(const uint8_t flags);
bool is_interlaced(const uint8_t flags);
bool is_lcl_sorted(const uint8_t flags);
int get_number_of_bits_in_lct(const uint8_t flags);

int read_sub_blocks(QDataStream &data_stream, std::vector<uint8_t> &data_blocks);
#endif // UTILS_H
