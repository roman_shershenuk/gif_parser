#ifndef SIMPLE_BITARRAY_H
#define SIMPLE_BITARRAY_H

#include <stdint.h>
#include <vector>

class SimpleBitArray {
public:
    SimpleBitArray(std::vector<uint8_t> &data_blocks);
    uint16_t get_bit(const size_t index);
    uint16_t get_bit_group(const size_t bit_n);
    size_t get_curt_pos();
private:
    std::vector<uint8_t> &m_data_blocks;
    size_t m_position;
};

#endif // SIMPLE_BITARRAY_H
