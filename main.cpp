#include <QFile>
#include <QDebug>

#include "utils.h"
#include "decoder.h"

int main(int argc, char *argv[]) {
    assert(argc == 3);

    QFile gif_file(argv[1]);
    int32_t file_open_result = gif_file.open(QIODevice::ReadOnly);

    assert(file_open_result != -1);

    QDataStream gif_data(&gif_file);

    // Чтение заголовка файла
    const int file_type_string_len = 7;
    char file_type_string[file_type_string_len];

    qDebug() << get_file_type_string(gif_data, file_type_string, file_type_string_len) << "bytes were received from GIF header" << '\n';
    qDebug() << "File type string:" << file_type_string << '\n';

    // Чтение дескриптора экрана
    Screen_Descritor_t screen_descriptor;
    qDebug() << get_screen_descriptor(gif_data, &screen_descriptor) << "bytes were received from GIF screen descriptor" << '\n';
    qDebug() << "-=Screen Descriptor=-";
    qDebug() << "Screen width in pixels:" << screen_descriptor.width;
    qDebug() << "Screen height in pixels:" << screen_descriptor.height;
    qDebug() << "Global color table:" << (is_gct(screen_descriptor.flags) ? "Yes" : "No");
    qDebug() << "Color resolution:" << get_color_res(screen_descriptor.flags);
    qDebug() << "Sorted flag:" << (is_sorted(screen_descriptor.flags) ? "Yes" : "No");
    qDebug() << "Number of bits in the global color table:" << get_number_of_bits_in_gct(screen_descriptor.flags);
    qDebug() << "Background color index:" << screen_descriptor.background_color_index;
    qDebug() << "Pixel aspect ratio:" << screen_descriptor.pixel_aspect_ratio << '\n';

    std::vector<uint8_t> global_color_table;

    // Если обнаружена глобальная цветовая таблица
    if(is_gct(screen_descriptor.flags)) {
        // Для RGB
        int global_color_table_len = 3 * get_number_of_bits_in_gct(screen_descriptor.flags);
        qDebug() << get_global_color_table(gif_data, global_color_table, global_color_table_len) << "bytes were received from GIF global color table" << '\n';
    }

    Image_Data_t image_data;

    parse_image_data(gif_data, image_data);
    qDebug() << "-=Image Descriptor=-";
    qDebug() << "Image left position:" << image_data.image_descriptor.image_left_position;
    qDebug() << "Image top position:" << image_data.image_descriptor.image_top_position;
    qDebug() << "Image width:" << image_data.image_descriptor.image_width;
    qDebug() << "Image height:" << image_data.image_descriptor.image_height;
    qDebug() << "Local color table:" << (is_lct(image_data.image_descriptor.flags) ? "Yes" : "No");
    qDebug() << "Interlaced image:" << (is_interlaced(image_data.image_descriptor.flags) ? "Yes" : "No");
    qDebug() << "Sorted flag:" << (is_lcl_sorted(image_data.image_descriptor.flags) ? "Yes" : "No");
    qDebug() << "Number of bits in the local color table:" << get_number_of_bits_in_lct(image_data.image_descriptor.flags) << '\n';

    std::vector<uint8_t> index_table;

    Decoder lzw_decoder(image_data, index_table);
    lzw_decoder.decode();

    QFile output_file(argv[2]);
    assert(output_file.open(QIODevice::WriteOnly) != false);
    QDataStream output_data(&output_file);

    for(size_t i = 0; i < index_table.size(); ++i) {
        if(is_lct(image_data.image_descriptor.flags)) {
            output_data.writeRawData((char*)&image_data.local_color_table[3 * index_table[i]], 1);
            output_data.writeRawData((char*)&image_data.local_color_table[3 * index_table[i] + 1], 1);
            output_data.writeRawData((char*)&image_data.local_color_table[3 * index_table[i] + 2], 1);
        }
        else {
            output_data.writeRawData((char*)&global_color_table[3 * index_table[i]], 1);
            output_data.writeRawData((char*)&global_color_table[3 * index_table[i] + 1], 1);
            output_data.writeRawData((char*)&global_color_table[3 * index_table[i] + 2], 1);
        }
    }
    output_file.close();


    return 0;
}
